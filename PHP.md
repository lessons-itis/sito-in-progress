# PHP - linguaggio di scripting lato server

Una guida PHP fatta bene: https://guidaphp.it

## 0 - Introduzione al PHP

I Programmi PHP vengono eseguiti sul server. (Non sul client)

Un file .php può contenere istruzioni PHP, HTML, CSS, JavaScript. Il codice PHP è infatti **incastonato** negli altri linguaggi.

Inizio del codice PHP: `<?php` (tutto attaccato)
Fine del codice PHP: `?>`

Esempio di pagina PHP:
```html
<!DOCTYPE html>
<html>
  <body>
  <?php
     echo "Hello world!";
  ?>
  </body>
</html>
```
Altro esempio più complesso:
```html
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
</head>
<body>
<?php
   // legge il contenuto di un file e lo mette in una variabile
   $filename = "password-condivisa.txt";
   $handle = fopen($filename, "r");
   $contents = fread($handle, filesize($filename));
   fclose($handle);

   // visualizza nella pagina di risposta la stringa letta
   echo "la password condivisa è $contents";
?>
</body>
</html>
```

### Informazioni varie
L'istruzione `echo` serve a generare contenuto per il body della risposta HTTP. A livello didattico in genere HTML, quindi può contenere i tag, ad esempio:
* `echo "ciao"` inserisce "ciao" nel body
* `echo "<h1>ciao</h1>` inserisce l'intestazione di livello 1 con contenuto "ciao"

**Le variabili iniziano con il simbolo $**
Non si può usare la variabile a, ma si deve usare $a. Ad esempio: $a, $num, $name

**Le variabili non vengono dichiarate. Non esistono i tipi**
`$a=3;`
`$a="ciao";` sono entrambi istruzioni lecite

**L'operatore "." è l'operatore di concatenazione delle stringhe**
Ad esempio:
* "Ciao"."mondo" concatena le scritte in "Ciaomondo"
* "Ciao "."mondo" concatena le scritte in "Ciao mondo"
```
$nome="Mario";
echo "Ciao ".$nome; //scrive "Ciao Mario"
```
**Ogni istruzione di PHP termina con il ;**

## 1 - Variabili
Abbiamo detto che le variabili devono essere precedute sempre dal simbolo `$`
Ad esempio: $a, $num, $name

Come altri linguaggi anche PHP prevede delle regole per la denominazione delle variabili:

-   Tutte le variabili iniziano con il **simbolo del dollaro** `$`, seguito dal nome della variabile.
-   Il nome della variabile deve iniziare con una lettera o un underscore `_`, i restanti caratteri possono essere lettere, numeri o underscore.
-   Il nome della variabile non può cominciare con un numero e non può contenere spazi.

Nonostante PHP sia un [linguaggio a tipizzazione debole](https://guidaphp.it/linguaggi-tipizzazione-forte-debole), supporta diversi tipi di dati scalari (booleani, numeri, stringhe) e composti (array e oggetti).


### Gli array

In PHP si possono definire gli array con la funzione `array`:
`$foods = array("Pizza", "Pollo", "Patate", "Carote")`
_Non_ si possono visualizzare con *echo*, ma **si possono visualizzare con** `print_r`, ad esempio:
`print_r($foods)`

Quando l'array viene stampato viene visualizzato tutto su una riga, **per visualizzarlo su più righe vedere il sorgente pagina** (con CTRL+U).

---
NOTA: è possibile settare nell'header della risposta HTTP il `Content-type` a `plain/text` con l'istruzione`header("Content-type: plain/text")`. In questo modo il browser non interpreta la risposta come HTML (`Content-type: text/html`), ma come testo semplice.

### Gli array associativi

**Molto importanti e utili sono gli array associativi**.
Tratto da https://guidaphp.it/base/array/associativi

Nel caso degli array associativi, gli elementi vengono individuati da un indice di tipo stringa, chiamato chiave (_key_), che stabilisce un associazione tra chiavi e valori.

```php
<?php
$array = [
    'nome' => 'Mario',
    'cognome' => 'Rossi',
    'eta' => '35',
    'lavoro' => 'programmatore'
];

echo $array['nome']."\n"; // Mario
echo "$array[lavoro]"; // programmatore

// l'istruzione echo "$array['lavoro']"; genera un errore di sintassi
```
L'associazione tra chiavi e valori viene fatta attraverso l'operatore di assegnamento `=>`, mentre è possibile riferirsi ad un elemento tramite la sua chiave.

**Nota:** quando si fa l'echo di una stringa tra doppi apici contenente l'elemento di un array associativo, è necessario riferirsi all'elemento omettendo gli apici della chiave, altrimenti il programma genera un errore di sintassi.

Un tipico utilizzo è con la funzione di PHP `in_array()`, che verifica la presenza di un elemento all'interno di un insieme di valori, come mostrato nell'esempio:

```php
<?php
$langs = [
    'it' => 'italiano',
    'en' => 'inglese',
    'es' => 'spagnolo',
    'fr' => 'francese',
    'de' => 'tedesco',
    'ru' => 'russo'
];

if (in_array('spagnolo', $langs)) {
    echo "Lingua supportata";
} else {
    echo "Lingua non supportata";
}
```

## 2 - Strutture di controllo

Le strutture di controllo supportate da PHP sono:

* **Istruzioni condizionali**: `if`, `else`,  `elseif`o `else if`, `switch`
* **Cicli iterativi**: `while`,  `do-while`,  `for`, `foreach`, `break`
* Le istruzioni:
	*  `break` che interrompe un ciclo o uno switch
	* `continue` che fa saltare l'iterazione a quella successiva
	* `include` e `require` utilizzati per includere opzionalmente (`include`), o obbligatoriamente (`require`) codice da altri files PHP
### if
Esempio:
```php
<?php
$x = 10;
$y = 14;

if ($x > $y) {
    echo "$x è maggiore di $y";
} else {
    echo "$x è minore o uguale di $y"; // 10 è minore o uguale di 14
}
```

Gli operatori di confronto sono:

* `==` Uguale a
* `===`Uguale e dello stesso tipo di
* `!=` oppure `<>` Non uguale a
* `!==` Non uguale e di tipo diverso di
* `<`,`>`,`<=`,`>=` Minore, Maggiore, Minore o uguale, Maggiore o uguale
* `<=>` Operatore _spaceship_ introdotto con PHP 7. Confronta `$x` e `$y` e restituisce  `1` se il `$x > $y`, `-1` se `$x < $y`, `0` se `$x == $y`.
* `??` Operatore _null coalesce_ introdotto con PHP 7. Utile per assegnare valori di default, confronta `$x`, `$y` e `$z` e restituisce il primo valore non `null`, se tutti i valori risultato `null` restituisce `null`.

Gli operatori logici sono:

* `&&` Operatore logico AND. La condizione è vera solo se entrambi gli operandi sono veri;
* `||`Operatore logico OR. La condizione è vera se almeno uno degli operandi è vero;
* `!`Operatore logico NOT. Inverte la logica dell'operando. Se è falso, la condizione è vera e viceversa;
* `xor` Operatore logico XOR. La condizione è vera solo se uno dei due operandi è vero.

### I cicli
#### while
```php
while (expr)
    statement
```
esempio:
```php
<?php
$money = 10;

while ($money > 0) {
    echo "Valore di \$money = $money\n";
    $money--;
}
```
#### for
c'è la versione analoga al C
```php
for (expr1; expr2; expr3)
    statement;
```
esempio
```php
<?php
$colors = ['blu', 'giallo', 'verde', 'rosso', 'bianco'];

for ($i = 0; $i < count($colors); $i++) {
    echo "$colors[$i]\n";
}
```
ottimizzazione: count() viene eseguito una volta sola perché tra le istruzioni iniziali

```php
<?php
$colors = ['blu', 'giallo', 'verde', 'rosso', 'bianco'];

for ($i = 0, $count = count($colors); $i < $count; $i++) {
    echo "$colors[$i]\n";
}
```
#### Foreach

Il costrutto foreach è comodo per gli **array associativi** e multidimensionali. Esiste in 2 versioni:
 - per array (monodimensionali)
```php
foreach ($array as $value)
    statement;
```

```php
<?php
$colors = ['blu', 'giallo', 'verde', 'rosso', 'bianco'];

// Prima versione

foreach ($colors as $color) {
    echo "$color\n";
}
?>```

- per array associativi (o multidimensionali)

```php
foreach ($array as $key => $value)
    statement;
```
```php
<?php
// Seconda versione

$days = [
    'lun' => 'lunedì',
    'mar' => 'martedì',
    'mer' => 'mercoledì',
    'gio' => 'giovedì',
    'ven' => 'venerdì',
    'sab' => 'sabato',
    'dom' => 'domenica'
];

foreach ($days as $key => $value) {
    echo "$key abbreviazione di $value\n";
}
?>
```

## 3 - Le variabili superglobali

https://guidaphp.it/base/variabili-superglobali

1.  `$_SERVER`: contiene tutto ciò che viene inviato dal server nella riposta HTTP.
2.  `$_GET`: contiene i parametri passati tramite il metodo `GET`.
3.  `$_POST`: contiene i parametri passati tramite il metodo `POST`.
4.  `$_REQUEST`: contiene i valori degli array `$_GET`, `$_POST` e `$_COOKIE`.
5.  `$_COOKIE`: contiene i [cookie](https://guidaphp.it/base/variabili-superglobali/cookie) inviati dal client al server.
6.  `$_SESSION`: contiene variabili registrate come [variabili di sessione](https://guidaphp.it/base/variabili-superglobali/sessioni).
7.  `$_FILES`: contiene informazioni sui file inviati tramite il metodo `POST`.


## 4 - Processare i dati di una form

### Pagina chiamante

```html
<html>
    <head>
    </head>
<body>
    <form action="pag2.php" method="GET">
        Inserisci il nome:<br>
            <input type="textbox" name="n">
            <input type="submit">
    </form>
</body>
</html>
```

Del tag form sono importanti:
* L’attributo method può assumere il valore “GET” o “POST”.
* Il valore dell’attributo action che rappresenta la pagina php chiamata

Della casella di testo è importante l’attributo name insieme al valore
assegnato.

### Pagina di risposta

La pag2.php prende il nome inviato dalla casella di testo e costruisce
la pagina di risposta scrivendo “Ciao “+ il nome di cui sopra.

```html
<html>
<head>
</head>
<body>
    <?php
        $nome=$_GET["n"];
        echo "Ciao ".$nome;
    ?>
</body>
</html>
```

L'istruzione fondamentale per prendere il dato spedito è : `$nome=$_GET["n"];`

Tale istruzione funziona se:

- La pag1.html usa il metodo GET ( `<form method=”GET”....>`)
- Il valore dell’attributo name della casella di testo si chiama n (`<input type="textbox" name="n">`)

Se viene usato il metodo POST ovvero è presente `<form method=”POST”....>` nel file pag1.html l’istruzione di lettura è:
`$nome=$_POST["n"];`

### Differenza tra POST e GET

* GET: concatena i nomi delle variabili e i suoi valori all’URL. `pag2.php?n=Mario`
* POST: I dati di cui sopra sono inviati nel body HTTP (e nella forma determinata dall'header `Content-encoding`)

NOTA: La risposta HTTP prodotta dalla richiesta della risorsa (file in questo caso) `pag2.php`
quando viene restituito al browser del client non contiene istruzioni PHP.
Infatti sul server, tali istruzioni vengono eseguite, viene generata la pagina HTML ed è solo questa che viene spedita al client.

Una differenza importante per scegliere se utilizzare una **GET** o una **POST** si ha nell'effetto che essa ha sul server:

* si usa una **GET** quando la richiesta non modifica lo stato dell'applicazione sul server (ad es: una ricerca);
* si usa una **POST** quando si modifica lo stato sul server (ad esempio: avvio di una sessione in un'operazione di login, oppure creazione di un record nel DB, upload di un file)



### Ciò che giunge al client è il seguente codice HTML:

```html
<html>
<head>
</head>
<body>
    Ciao Mario
</body>
</html>
```

Mario è la parte dinamica di questo esercizio


## 5 - Restituire dati in formato JSON

[V. Guida PHP fino a Codificare dati JSON compreso](https://guidaphp.it/base/gestione-file/json)


