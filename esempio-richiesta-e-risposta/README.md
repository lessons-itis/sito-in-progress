# Richieste HTTP con curl

`curl` è programma molto potente per fare richieste da righe di comando

## Richiesta base:

curl http://nao.iismerlonimiliani.it:59000/api/v1/papers/

## Visualizzare a video gli header di risposta

curl -v http://nao.iismerlonimiliani.it:59000/api/v1/papers/

## Salvare il body della risposta nel file `answer.txt`

curl -v http://nao.iismerlonimiliani.it:59000/api/v1/papers/ -o answer.txt

## Salvare anche gli header della risposta nel file `headers.txt`

curl -v http://nao.iismerlonimiliani.it:59000/api/v1/papers/ -o answer.txt -D headers.txt

## Mettere intestazioni e body in un file unico

cat headers.txt answer.txt > response.txt
