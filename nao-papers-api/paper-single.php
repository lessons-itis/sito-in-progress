<?php

if (!isset($_GET["sheet"])) {
    /* PARTE 1 -- Parametro non specificato nel query string */
    http_response_code(400);
    echo "Devi impostare il parametro 'sheet' nel querystring per utilizzare questa API";

} else {

    /* PARTE 2 -- parametro specificato */
    $num=$_GET["sheet"];

    $values=array(
        ["city" => "Fabriano",
         "kind" => "Carta vergata",
         "id" => 1,
         "author"=> "Indirizzo Carta presso I.I.S. Merloni Miliani"],
        ["city" => "Fabriano",
         "kind"=> "Carta ",
         "id"=> 2,
         "author"=> "Autore 2"],
        ["city"=> "Fabriano",
         "kind"=> "Carta vergata",
         "id"=> 3,
         "author"=> "Autore 3"]
    );

    $i = 0;
    $find = FALSE;

    foreach ($values as $value) 
    {
        $i += 1;
        if ($value["id"] == $num) 
	{
	    $find = TRUE;
            break;
        } 
    }

    if (!$find) 
    {
            /* PARTE 3 -- foglio non trovato */
            http_response_code(404);
?>
            <html><head>
                <style type="text/css">.red {color: red}</style>
            </head><body>
                <h1 class="red">Foglio di carta non trovato, stavi cercando di rompermi con un valore fasullo?</h1>
            </body></html>
<?php
            return;
    }

    /* PARTE 4 -- restituzione normale del dato trovato in formato JSON */
    header("Content-type: application/json");
    $json=json_encode($value);

    echo $json;
}

?>
